// Wifi
////////////////////////////////////////////////////////////////////////////////
//
// Libary for Wifi connection for the ESP2866
//
////////////////////////////////////////////////////////////////////////////////
// By Michael Egtved Christensen 2017-2019
////////////////////////////////////////////////////////////////////////////////
//
//  Object wrapping of the ESP8266WiFi.h module
//  Connects to the first available SSID list of wifi's
//  Made in attempt to be robust, reconnects if necessary.
//
//  setup 				-> include in main setup
//  loop				-> include loop in main loop
//
//  INTERNAL:
//
//  NOTE:
//
//  For usage see reference implementation
//  max number of networks is 10 (ignores the rest)
//
////////////////////////////////////////////////////////////////////////////////
#ifndef MecWifi_h
#define MecWifi_h

#include <Arduino.h>
#if defined(ESP8266)
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <FS.h> // Filesystem
#elif defined(ESP32)
#include <WiFi.h>
#include <ESPmDNS.h>

#include "SPIFFS.h"
#endif

#include <DNSServer.h>
#include <MecLogging.h>

class MecWifi
{
public:
  MecWifi(String WiFiSSID[], String WiFiPassword[], int WifiCount, MecLogging(*MyLog), String mDNSName, boolean RunSoftAP = true,
          String tAPPassword = "D0gshit1!"); // password mindst 8 Karaktere på ESP32
  void setup();
  void loop();
  String LongWifiStatus();

  String WiFiSSID[10];
  String WiFiPassword[10];
  String mDNSName;
  int WifiCount;
  boolean RunSoftAP;
  String APPassword;

private:
  MecLogging *_MyLog;
  int _WifiWaitPeriod = 30000;
  int _NextWifiCheck = 0;
};

#endif
