// Wifi
/**
* 
////////////////////////////////////////////////////////////////////////////////
//
// Libary for Wifi connection for the ESP2866
//
////////////////////////////////////////////////////////////////////////////////
// By Michael Egtved Christensen 2017-2020
////////////////////////////////////////////////////////////////////////////////
//
//  Object wrapping of the ESP8266WiFi.h module
//  Connects to the first available SSID list of wifi's
//  Made in attempt to be robust, reconnects if necessary.
//
//  setup 				-> include in main setup
//  loop				-> include loop in main loop
//
//  INTERNAL:
//
//  NOTE:
//
//
//  For usage see reference implementation
//  max number of networks is 10 (ignores the rest)
//
//  mDNS:
//       https://tttapa.github.io/ESP8266/Chap08%20-%20mDNS.html
//       https://github.com/esp8266/Arduino/blob/master/libraries/ESP8266WebServer/examples/HelloServer/HelloServer.ino

////////////////////////////////////////////////////////////////////////////////
*/
#include <MecWifi.h>

//Static IP address configuration
IPAddress staticIP(192, 168, 1, 221); //ESP static ip
IPAddress gateway(192, 168, 1, 1);    //IP Address of your WiFi Router (Gateway)
IPAddress subnet(255, 255, 255, 0);   //Subnet mask
IPAddress dns(8, 8, 8, 8);            //DNS

DNSServer dnsServer;

////////////////////////////////////////////////////////////////////////////////
// File storage
////////////////////////////////////////////////////////////////////////////////

#define WiFI_CONFIG_FILE "/WiFICONFIGFILE"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//
// Wifi
//
//
////////////////////////////////////////////////////////////////////////////////
MecWifi::MecWifi(String tWiFiSSID[], String tWiFiPassword[], int tWifiCount, MecLogging(*MyLog), String tmDNSName, boolean tRunSoftAP /* = true*/, String tAPPassword /*= "D0gshit1!"*/)
{
  // max number of networks is 10 (ignores the rest)
  if (tWifiCount > 10)
  {
    tWifiCount = 10;
  }

  // copy's the ssid's and passwords to local array
  for (int n; n < tWifiCount; n++)
  {
    WiFiSSID[n] = tWiFiSSID[n];
    WiFiPassword[n] = tWiFiPassword[n];
  }

  WifiCount = tWifiCount;
  mDNSName = tmDNSName;
  RunSoftAP = tRunSoftAP;
  APPassword = tAPPassword;
  _MyLog = MyLog;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//
// Loop
//
//////////////////////////////////////////////////////}//////////////////////////

void Ydelay(unsigned long ms)
{
  uint32_t start = micros();

  while (ms > 0)
  {
    yield();
    while (ms > 0 && (micros() - start) >= 1000)
    {
      ms--;
      start += 1000;
    }
  }
}

void MecWifi::loop()
{
  // debug tekst
  (*_MyLog).infoNl("WiFiI: WifiInit start", Lib);

  // Connected?
  if (_NextWifiCheck < millis())
  {
    (*_MyLog).infoNl("WiFiI: Time for Next WIFI Check", Deb);
    if (WiFi.status() != WL_CONNECTED)
    {
      //Loop gennem de enkelte netværk for at connecte
      for (int Nr = 0; Nr < WifiCount; Nr++)
      {
        (*_MyLog).infoNl("WifiI: Connecter til " + String(WiFiSSID[Nr]), Inf);

        WiFi.persistent(false);
        WiFi.disconnect();
        WiFi.mode(WIFI_OFF);
        WiFi.mode(WIFI_STA);

        //        WiFi.disconnect(); //Prevent connecting to wifi based on previous configuration
        WiFi.mode(WIFI_AP_STA);
#if defined(ESP8266)
        WiFi.hostname(mDNSName); // DHCP Hostname (useful for finding device for static lease)
#elif defined(ESP32)
        (*_MyLog).infoNl("ESP32", Inf);
        WiFi.setHostname(mDNSName.c_str()); // DHCP Hostname (useful for finding device for static lease)
#endif

        WiFi.begin(WiFiSSID[Nr].c_str(), WiFiPassword[Nr].c_str());

        (*_MyLog).infoNl("WifiI: ", Inf);
        for (int t = 0; t < 70; t++)
        {
          (*_MyLog).infoAppend(".", Inf);
          (*_MyLog).infoAppend(String(WiFi.status()), Inf);
          // WL_IDLE_STATUS      = 0,
          // WL_NO_SSID_AVAIL    = 1,
          // WL_SCAN_COMPLETED   = 2,
          // WL_CONNECTED        = 3,
          // WL_CONNECT_FAILED   = 4,
          // WL_CONNECTION_LOST  = 5,
          // WL_DISCONNECTED     = 6
          // https://forum.arduino.cc/index.php?topic=506133.0

          Ydelay(200);
          (*_MyLog).infoNl(String(WiFi.status()), Lib);

          if (WiFi.status() == WL_CONNECTED)
          {
            (*_MyLog).infoNl("", Inf);
            (*_MyLog).infoNl("WiFiI: Connected", Inf);
            (*_MyLog).infoNl("WiFiI: IP address " + WiFi.localIP().toString(), Inf);

#if defined(ESP8266)
            if (!MDNS.begin(mDNSName, WiFi.localIP()))
#elif defined(ESP32)
            const char *tmp = mDNSName.c_str();
            if (!MDNS.begin(tmp))
#endif
            { // Start the mDNS responder for esp8266.local
              (*_MyLog).infoNl("Error setting up MDNS responder!", Sto);
            }
            else
            {
              (*_MyLog).infoNl("mDNS responder started", Inf);
            }
            Nr = 999;
            break;
          }

          if (WiFi.status() == WL_NO_SSID_AVAIL || WiFi.status() == WL_CONNECT_FAILED)
          {
            (*_MyLog).infoNl("", Inf);
            (*_MyLog).infoNl("WiFiI: Connection Faild", Sto);
            (*_MyLog).infoNl(WiFiSSID[Nr], Sto);
            (*_MyLog).infoNl(String(WiFi.status()), Sto);
            //Nr++;
            break;
          };
        };
        (*_MyLog).infoNl("", Inf);
        // break;
      };

      if (_NextWifiCheck == 0)
      { // first run of this loop

        if (RunSoftAP == true)
        {
          (*_MyLog).infoNl("Starting SoftAP: " + mDNSName + " pass: " + APPassword + " ", Inf);
          WiFi.softAP(mDNSName.c_str(), APPassword.c_str()); //password must be 8 chars long
          (*_MyLog).infoNl("Soft APIP :" + WiFi.softAPIP().toString(), Deb);
          dnsServer.stop();
          dnsServer.setErrorReplyCode(DNSReplyCode::NoError);
          dnsServer.start(53, "*", WiFi.softAPIP());
        }
      }

      if (WiFi.status() == WL_CONNECTED)
      {
        (*_MyLog).infoNl("WifiI: Forbundet til " + WiFi.SSID(), Deb);
        _WifiWaitPeriod = 60000;
        _NextWifiCheck = millis() + _WifiWaitPeriod;
      }
      else
      {
        (*_MyLog).infoNl("WiFiI: WiFi still NOT Connectet wating " + String(_WifiWaitPeriod * 1.5 / 60000), Deb);
        _WifiWaitPeriod = _WifiWaitPeriod * 2;
        if (_WifiWaitPeriod > (2 * 60 * 60 * 1000))
        {
          (*_MyLog).infoNl("_WifiWaitPeriod:" + String(_WifiWaitPeriod), Deb);
          _WifiWaitPeriod = 2 * 60 * 60 * 1000;
        }
        _NextWifiCheck = millis() + _WifiWaitPeriod;
      };
    }
    else
    {
      (*_MyLog).infoNl("WiFiI: Wifi already connected", Lib);
      _WifiWaitPeriod = 60000;
      _NextWifiCheck = millis() + _WifiWaitPeriod;
    }
  };

  if (RunSoftAP == true)
  {
    dnsServer.processNextRequest();
  }

#if defined(ESP8266)
  MDNS.update();
#elif defined(ESP32)

#endif

  (*_MyLog).infoNl("WiFiI: WifiInit end", Lib);
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//
// LongWifiStatus
//
//////////////////////////////////////////////////////}//////////////////////////

String MecWifi::LongWifiStatus()
{
  String StatusString = "WIFI STATUS: \n\n";

  if (WiFi.status() == WL_CONNECTED)
  {
    StatusString = StatusString + " Connected\n";
    StatusString = StatusString + " IP address: " + WiFi.localIP().toString() + "\n";
    StatusString = StatusString + " mDNS name: " + mDNSName + ".local\n";
  }
  else
  {
    StatusString = StatusString + "Connection Faild\n";
  }

  StatusString = StatusString + "\n\nSOFT AP STATUS: \n\n";
  if (RunSoftAP == true)
  {
    StatusString = StatusString + " Accespunkt: " + mDNSName + "\n";
    StatusString = StatusString + " Password: " + APPassword + "\n";
    StatusString = StatusString + " IP address: " + WiFi.softAPIP().toString() + "\n\n";
  }
  else
  {
    StatusString = StatusString + " Disabled \n";
  }

  return StatusString;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//
// setup
//
//////////////////////////////////////////////////////}//////////////////////////

void MecWifi::setup()
{

  int calDataOK = 0;

  // check file system exists
  if (!SPIFFS.begin())
  {
    (*_MyLog).infoNl("WiFiI:SPIFFS: Formating file system", War);
    bool formatted = SPIFFS.format();
    if (formatted)
    {
      (*_MyLog).infoNl("WiFiI:SPIFFS: Success formatting", Inf);
    }
    else
    {
      (*_MyLog).infoNl("WiFiI:SPIFFS: Error formatting", Sto);
    }
    SPIFFS.begin();
  }
  else
  {
    (*_MyLog).infoNl("WiFiI:PIFFS: is formatted. Moving along...", Inf);
  }

  if (SPIFFS.exists(WiFI_CONFIG_FILE))
  {
    (*_MyLog).infoNl("WIFI:SPIFFS: Reading Calibration file", Inf);
    File f = SPIFFS.open(WiFI_CONFIG_FILE, "r");
    if (f)
    {
      char buffer[64];
      int l = f.readBytesUntil('\n', buffer, sizeof(buffer));
      buffer[l] = 0;
      (*_MyLog).infoNl("READ FILE: mDNSName: " + String(buffer), Inf);
      mDNSName = String(buffer);

      l = f.readBytesUntil('\n', buffer, sizeof(buffer));
      buffer[l] = 0;
      (*_MyLog).infoNl("READ FILE: APPassword: " + String(buffer), Inf);
      APPassword = String(buffer);

      l = f.readBytesUntil('\n', buffer, sizeof(buffer));
      buffer[l] = 0;
      (*_MyLog).infoNl("READ FILE: RunSoftAP: " + String(buffer), Inf);
      if (String(buffer) == "true")
      {
        RunSoftAP = true;
      }
      else
      {
        RunSoftAP = false;
      }

      int n = 0;
      while (n < 10)
      {
        // WiFi AP
        l = f.readBytesUntil('\n', buffer, sizeof(buffer));
        buffer[l] = 0;
        (*_MyLog).infoNl("READ FILE: WiFISSID" + String(n) + ": " + String(buffer), Inf);
        if (String(buffer) != "")
        {
          WiFiSSID[n] = String(buffer);
          // Wifi Password
          l = f.readBytesUntil('\n', buffer, sizeof(buffer));
          buffer[l] = 0;
          //( * _MyLog).infoNl("READ FILE: WiFiPassword" + String(n) + ": " + String(buffer), Inf);
          WiFiPassword[n] = String(buffer);
        }
        else
        {
          //( * _MyLog).infoNl("What THE FUCK n=" + String(n), Inf);
          WifiCount = n;
          n = 99;
        }
        n++;
      }

      f.close();
    }

    f = SPIFFS.open(WiFI_CONFIG_FILE, "r");

    while (f.available())
    {
      char buffer[64];
      int l = f.readBytesUntil('\n', buffer, sizeof(buffer));
      buffer[l] = 0;
      (*_MyLog).infoNl(buffer, Inf);
    }
    //if (f.readBytes((char *)calData, 14) == 14)
    //  calDataOK = 1;
    f.close();
  }

  loop();
}

/* Serial.println(WL_CONNECTED); 3
  Serial.println(WL_NO_SHIELD); 255
  Serial.println(WL_IDLE_STATUS); 0
  Serial.println(WL_NO_SSID_AVAIL); 1
  Serial.println(WL_SCAN_COMPLETED); 2
  Serial.println(WL_CONNECT_FAILED); 4
  Serial.println(WL_CONNECTION_LOST); 5
  Serial.println(WL_DISCONNECTED); 6 */
